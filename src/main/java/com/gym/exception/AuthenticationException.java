package com.gym.exception;

/**
 * Created by Joanna Olejnik on 2017-05-27.
 */
public class AuthenticationException extends RuntimeException {
    public AuthenticationException() {
        super();
    }
}
