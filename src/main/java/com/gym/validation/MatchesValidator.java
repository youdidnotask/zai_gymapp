package com.gym.validation;

import com.gym.model.User;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Joanna Olejnik on 2017-05-24.
 */
public class MatchesValidator implements ConstraintValidator<Matches, Object> {

    private String property;
    private String verifier;

    @Override
    public void initialize(Matches annotation) {
        this.property = annotation.property();
        this.verifier = annotation.verifier();
    }

    @Override
    public boolean isValid(Object target, ConstraintValidatorContext context) {
        if(target == null) {
            return true;
        }

        boolean matches = true;
        BeanWrapper beanWrapper = PropertyAccessorFactory.forBeanPropertyAccess(target);

        Object propertyObject = beanWrapper.getPropertyValue(property);
        Object verifierObject = beanWrapper.getPropertyValue(verifier);

        if(propertyObject==null && verifierObject==null) {
            matches = true;
        }
        else if(!propertyObject.equals(verifierObject)) {
            addConstraintViolation(property, verifier, context);
            matches = false;
        }

        return matches;
    }

    private void addConstraintViolation(String property, String verifier, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        context
                .buildConstraintViolationWithTemplate("passwords do not match")
                .addPropertyNode(verifier)
                .addConstraintViolation();
    }
}

//public class MatchesValidator
//        implements ConstraintValidator<Matches, Object> {
//
//    @Override
//    public void initialize(Matches constraintAnnotation) {
//    }
//    @Override
//    public boolean isValid(Object obj, ConstraintValidatorContext context){
//        User user = (User) obj;
//        return user.getPassword().equals(user.getPasswordConfirm());
//    }
//}
