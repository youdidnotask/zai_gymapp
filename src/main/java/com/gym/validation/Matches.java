package com.gym.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by Joanna Olejnik on 2017-05-24.
 */

@Documented
@Constraint(validatedBy = MatchesValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Matches {
    String message() default "values do not match";
    String property();
    String verifier();
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

//@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
//@Retention(RetentionPolicy.RUNTIME)
//@Constraint(validatedBy = MatchesValidator.class)
//@Documented
//public @interface Matches {
//    String message() default "Passwords don't match";
//    Class<?>[] groups() default {};
//    Class<? extends Payload>[] payload() default {};
//}
