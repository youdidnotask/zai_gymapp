package com.gym.config;

import com.gym.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.TemplateResolver;

/**
 * Created by Joanna Olejnik on 2017-05-22.
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/register").permitAll()
                    .antMatchers("/schedule").authenticated()
                    .antMatchers("/schedule/signup").hasRole(Role.USER.name())
                    .antMatchers("/schedule/signout").hasRole(Role.USER.name())
                    .antMatchers("/activity/my").hasRole(Role.USER.name())
                    .antMatchers("/schedule/delete").hasAnyRole(Role.EMPLOYEE.name(), Role.ADMIN.name())
                    .antMatchers("/activity/add").hasAnyRole(Role.EMPLOYEE.name(), Role.ADMIN.name())
                    .antMatchers("/clients").hasRole(Role.ADMIN.name())
                    .antMatchers("/profile").hasAnyRole(Role.USER.name(), Role.EMPLOYEE.name())
                    .antMatchers("/employees").hasRole(Role.ADMIN.name())
                    .antMatchers("/employee/add").hasRole(Role.ADMIN.name())
                    .anyRequest().permitAll()
                    .and()
                .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .defaultSuccessUrl("/schedule")
                    .and()
                .exceptionHandling()
                    .accessDeniedPage("/accessDenied")
                    .and()
                .csrf();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

}
