package com.gym.controller;

import com.gym.model.Role;
import com.gym.model.User;
import com.gym.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Joanna Olejnik on 2017-05-18.
 */

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final UserService userService;

    @Autowired
    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String loadRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping
    public String registerUser(@Valid User user, BindingResult bindingResult) {
        if(bindingResult.hasFieldErrors("username") ||
                bindingResult.hasFieldErrors("mail") ||
                bindingResult.hasFieldErrors("passwordTmp") ||
                bindingResult.hasFieldErrors("passwordConfirmTmp")) {
            return "register";
        }

        user.setPassword(user.getPasswordTmp());
        userService.save(user, Role.USER);
        return "home";
    }
}
