package com.gym.controller;

import com.gym.model.Role;
import com.gym.model.User;
import com.gym.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by Joanna Olejnik on 2017-05-25.
 */
@Controller
public class ClientsController {

    private final UserService userService;

    @Autowired
    public ClientsController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/clients")
    public String loadClientsPage(
            @RequestParam(value="phrase", defaultValue = "") String phrase,
            Model model,
            Principal principal) {

        User user = userService.getLoggedInUser(principal);
        model.addAttribute("username", user.getUsername());
        if(user.getAvatar()!=null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }

        model.addAttribute("page", 0);

        Page<User> users = (phrase.equals("")) ? userService.findUsers(Role.USER, 0) : userService.findUsers(Role.USER, phrase, 0);

        model.addAttribute("users", users);
        model.addAttribute("has_prev", false);
        model.addAttribute("has_next", users.hasNext());
        return "clients";
    }

    @GetMapping(value = "/clients/page/{page}")
    public String loadClientsPage(
            @PathVariable("page") int page,
            @RequestParam(value="phrase", defaultValue = "") String phrase,
            Model model,
            Principal principal){

        User user = userService.getLoggedInUser(principal);
        model.addAttribute("username", user.getUsername());
        if(user.getAvatar()!=null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }

        page = (page<0) ? 0 : page;
        model.addAttribute("page", page);

        Page<User> users = (phrase.equals("")) ? userService.findUsers(Role.USER, page) : userService.findUsers(Role.USER, phrase, page);

        model.addAttribute("users", users);
        model.addAttribute("has_prev", users.hasPrevious());
        model.addAttribute("has_next", users.hasNext());
        return "clients";
    }

    @PostMapping(value="/clients")
    public String deleteClient(@RequestParam(value="user_id") Long id) {
        userService.delete(id);
        return "redirect:/clients";
    }
}
