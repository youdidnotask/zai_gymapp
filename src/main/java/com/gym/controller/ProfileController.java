package com.gym.controller;

import com.gym.model.User;
import com.gym.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Base64;

/**
 * Created by Joanna Olejnik on 2017-05-27.
 */

@Controller
public class ProfileController {

    private final UserService userService;

    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/profile")
    public String loadProfilePage(Principal principal, Model model)  {
        User user = userService.getLoggedInUser(principal);
        model.addAttribute("username", user.getUsername());
        model.addAttribute("mail", user.getMail());
        if(user.getAvatar() != null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }
        return "profile";
    }

    @PostMapping(value = "/profile/avatar")
    public String addAvatar (
            @RequestParam(value="image") MultipartFile file,
            Principal principal) throws IOException {
        User user = userService.getLoggedInUser(principal);
        user.setAvatar(file);
        userService.save(user);
        return "redirect:/profile";
    }

    @PostMapping(value = "/profile/mail")
    public String addAvatar (
            @RequestParam(value="mail") String mail,
            Principal principal) throws IOException {
        User user = userService.getLoggedInUser(principal);
        user.setMail(mail);
        userService.save(user);
        return "redirect:/profile";
    }
}
