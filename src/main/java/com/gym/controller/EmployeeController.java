package com.gym.controller;

import com.gym.model.Role;
import com.gym.model.User;
import com.gym.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by Joanna Olejnik on 2017-05-29.
 */
@Controller
public class EmployeeController {

    private final UserService userService;

    @Autowired
    public EmployeeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/employees")
    public String loadEmployeesPage(
            @RequestParam(value="phrase", defaultValue = "") String phrase,
            Model model,
            Principal principal) {

        User user = userService.getLoggedInUser(principal);
        model.addAttribute("username", user.getUsername());
        if(user.getAvatar()!=null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }

        model.addAttribute("page", 0);

        Page<User> users = (phrase.equals("")) ? userService.findUsers(Role.EMPLOYEE, 0) : userService.findUsers(Role.EMPLOYEE, phrase, 0);

        model.addAttribute("users", users);
        model.addAttribute("has_prev", false);
        model.addAttribute("has_next", users.hasNext());
        return "employees";
    }

    @GetMapping(value = "/employees/page/{page}")
    public String loadEmployeesPage(
            @PathVariable("page") int page,
            @RequestParam(value="phrase", defaultValue = "") String phrase,
            Model model,
            Principal principal){

        User user = userService.getLoggedInUser(principal);
        model.addAttribute("username", user.getUsername());
        if(user.getAvatar()!=null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }

        page = (page<0) ? 0 : page;
        model.addAttribute("page", page);

        Page<User> users = (phrase.equals("")) ? userService.findUsers(Role.EMPLOYEE, page) : userService.findUsers(Role.EMPLOYEE, phrase, page);

        model.addAttribute("users", users);
        model.addAttribute("has_prev", users.hasPrevious());
        model.addAttribute("has_next", users.hasNext());
        return "employees";
    }

    @PostMapping(value="/employees")
    public String deleteEmployee(@RequestParam(value="user_id") Long id) {
        userService.delete(id);
        return "redirect:/employees";
    }


    @GetMapping(value = "/employee/add")
    public String loadAddEmployeePage(Principal principal, Model model) {
        User user = userService.getLoggedInUser(principal);
        model.addAttribute("username", user.getUsername());
        if(user.getAvatar()!=null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }
        model.addAttribute("user", new User());
        return "employee/add";
    }

    @PostMapping(value = "/employee/add")
    public String registerEmployee(@Valid User user, BindingResult bindingResult) {
        if(bindingResult.hasFieldErrors("username") ||
                bindingResult.hasFieldErrors("mail") ||
                bindingResult.hasFieldErrors("passwordTmp") ||
                bindingResult.hasFieldErrors("passwordConfirmTmp")) {
            return "employee/add";
        }

        user.setPassword(user.getPasswordTmp());
        userService.save(user, Role.EMPLOYEE);
        return "redirect:/employees";
    }
}
