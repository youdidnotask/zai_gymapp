package com.gym.controller;

import com.gym.model.Activity;
import com.gym.model.Day;
import com.gym.model.User;
import com.gym.service.ActivityService;
import com.gym.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Joanna Olejnik on 2017-05-23.
 */

@Controller
public class ScheduleController {

    private final ActivityService activityService;

    private final UserService userService;

    @Autowired
    public ScheduleController(ActivityService activityService, UserService userService) {
        this.activityService = activityService;
        this.userService = userService;
    }

    @GetMapping(value = "/schedule")
    public String loadSchedulePage(Principal principal, Model model) {
        User user = userService.getLoggedInUser(principal);
        model.addAttribute("user", user);
        model.addAttribute("username", user.getUsername());
        if(user.getAvatar()!=null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }

        List<Activity> activityMon = activityService.findByDay(Day.MONDAY);
        List<Activity> activityTue = activityService.findByDay(Day.TUESDAY);
        List<Activity> activityWed = activityService.findByDay(Day.WEDNESDAY);
        List<Activity> activityThu = activityService.findByDay(Day.THURSDAY);
        List<Activity> activityFri = activityService.findByDay(Day.FRIDAY);
        List<List<Activity>> activityAll = new ArrayList<>(Arrays.asList(activityMon, activityTue, activityWed, activityThu, activityFri));
        model.addAttribute("activityAll", activityAll);
        return "schedule";
    }

    @PostMapping(value = "/schedule/delete")
    public String deleteActivity(@RequestParam(value="activity_id") Long id) {
        activityService.delete(id);
        return "redirect:/schedule";
    }

    @PostMapping(value = "/schedule/signup")
    public String signUpForActivity(
            @RequestParam(value="activity_id") Long id,
            Principal principal) {

        User user = userService.getLoggedInUser(principal);
        user.addActivity(activityService.find(id));
        userService.save(user);
        return "redirect:/schedule";
    }

    @PostMapping(value = "/schedule/signout")
    public String signOutFromActivity(
            @RequestParam(value="activity_id") Long id,
            Principal principal) {

        User user = userService.getLoggedInUser(principal);
        user.deleteActivity(id);
        userService.save(user);
        return "redirect:/schedule";
    }

    @GetMapping(value = "/activity/add")
    public String loadAddActivityPage(Principal principal, Model model) {
        User user = userService.getLoggedInUser(principal);
        model.addAttribute("username", user.getUsername());
        if(user.getAvatar()!=null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }
        return "activity/add";
    }

    @PostMapping(value = "/activity/add")
    public String saveActivity(
            @RequestParam(value="name") String name,
            @RequestParam(value="hour") String hour,
            @RequestParam(value="day") Day day) {

        Activity activity = new Activity();
        activity.setName(name);
        activity.setHour(hour);
        activity.setDay(day);
        activityService.save(activity);
        return "redirect:/schedule";
    }

    @GetMapping(value = "/activity/my")
    public String loadMyActivitiesPage(Principal principal, Model model) {
        User user = userService.getLoggedInUser(principal);
        model.addAttribute("username", user.getUsername());
        if(user.getAvatar()!=null) {
            model.addAttribute("avatar", user.getAvatar_Base64());
        }
        model.addAttribute("activities", user.getActivitiesInOrder());
        return "activity/my";
    }

    @PostMapping(value = "/activity/my")
    public String signOutFromMyActivity(
            @RequestParam(value="activity_id") Long id,
            Principal principal) {

        User user = userService.getLoggedInUser(principal);
        user.deleteActivity(id);
        userService.save(user);
        return "redirect:/activity/my";
    }
}
