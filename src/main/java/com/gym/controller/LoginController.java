package com.gym.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Joanna Olejnik on 2017-05-15.
 */

@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public String loadLoginPage() {
        return "login";
    }

}