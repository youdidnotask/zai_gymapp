package com.gym.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Joanna Olejnik on 2017-05-15.
 */

@Controller
public class AppController {

    @GetMapping(value = "/")
    public String loadHomePage() {
        return "home";
    }

    @GetMapping(value = "/accessDenied")
    public String loadAccessDeniedPage() {
        return "accessDenied";
    }
}
