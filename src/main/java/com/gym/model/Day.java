package com.gym.model;

/**
 * Created by Joanna Olejnik on 2017-05-26.
 */
public enum Day {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY
}
