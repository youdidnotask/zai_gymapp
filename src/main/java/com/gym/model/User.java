package com.gym.model;

import com.gym.validation.Matches;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by Joanna Olejnik on 2017-05-18.
 */

@Entity
@Table(name = "users")
@Matches(property = "passwordTmp", verifier = "passwordConfirmTmp")
public class User implements UserDetails {
    @Id
    @GeneratedValue
    private Long id;

    @Size(min=3, max=16)
    private String username;

    @Size(min=3, max=64)
    private String mail;

    @Transient
    @Size(min=3, max=16)
    private String passwordTmp;

    @Transient
    private String passwordConfirmTmp;

    @NotNull
    private String password;

    @Lob
    private byte[] avatar;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( name="user_authority",
                joinColumns = @JoinColumn(name="user_id"),
                inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Set<Authority> authorities = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( name="user_activity",
            joinColumns = @JoinColumn(name="user_id"),
            inverseJoinColumns = @JoinColumn(name = "activity_id"))
    private Set<Activity> activities = new HashSet<>();

    private boolean enabled = true;

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPasswordTmp() {
        return passwordTmp;
    }

    public void setPasswordTmp(String passwordTmp) {
        this.passwordTmp = passwordTmp;
    }

    public String getPasswordConfirmTmp() {
        return passwordConfirmTmp;
    }

    public void setPasswordConfirmTmp(String passwordConfirmTmp) {
        this.passwordConfirmTmp = passwordConfirmTmp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public String getAvatar_Base64() {
        return Base64.getEncoder().encodeToString(avatar);
    }

    public boolean setAvatar(MultipartFile file) throws IOException {
        this.avatar = file.getBytes();
        return true;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public Set<Activity> getActivities() {
        return activities;
    }

    public Set<Activity> getActivitiesInOrder() {
        TreeSet<Activity> activitiesTreeSet = new TreeSet<>(Activity.activityComparator);
        activitiesTreeSet.addAll(activities);
        return activitiesTreeSet;
    }

    public void setActivities(Set<Activity> activities) {
        this.activities = activities;
    }

    public void addActivity(Activity activity) {
        if(!this.activities.contains(activity)) {
            this.activities.add(activity);
        }
    }

    public void deleteActivity(Long id) {
        for(Activity activity : activities) {
            if(activity.getId() == id) {
                this.activities.remove(activity);
                return;
            }
        }
    }

    public boolean hasActivity(Long id) {
        for(Activity activity : activities) {
            if(activity.getId() == id) {
                return true;
            }
        }
        return false;
    }
}
