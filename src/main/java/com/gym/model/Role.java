package com.gym.model;

/**
 * Created by Joanna Olejnik on 2017-05-22.
 */
public enum Role {
    ADMIN,
    EMPLOYEE,
    USER
}
