package com.gym.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Joanna Olejnik on 2017-05-26.
 */

@Entity
@Table(name="activities")
public class Activity {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String name;

    @Enumerated(EnumType.STRING)
    private Day day;

    @NotNull
    private String hour;

    @ManyToMany(mappedBy = "activities")
    private Set<User> users = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public static Comparator<Activity> activityComparator = new Comparator<Activity>() {
        @Override
        public int compare(Activity a1, Activity a2) {
            if(a1.getDay()==a2.getDay()) {
                return a1.getHour().compareTo(a2.getHour());
            }
            else {
                return a1.getDay().compareTo(a2.getDay());
            }
        }
    };
}
