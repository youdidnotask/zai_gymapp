package com.gym.service;

import com.gym.model.Activity;
import com.gym.model.Role;
import com.gym.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;

/**
 * Created by Joanna Olejnik on 2017-05-18.
 */
public interface UserService {
    void save(User user);
    void save(User user, Role role);
    void delete(Long id);
    User find(Long id);
    User findByUsername(String username);
    Page<User> findUsers(Role role, int page);
    Page<User> findUsers(Role role, String phrase, int page);
    Iterable<User> findAll();
    User getLoggedInUser(Principal principal);
}
