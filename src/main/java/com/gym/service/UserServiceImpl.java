package com.gym.service;

import com.gym.exception.AuthenticationException;
import com.gym.model.Activity;
import com.gym.model.Role;
import com.gym.model.User;
import com.gym.repository.AuthorityRepository;
import com.gym.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Joanna Olejnik on 2017-05-18.
 */

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    private final static int PAGE_SIZE=8;

    private final UserRepository userRepository;

    private final AuthorityRepository authorityRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AuthorityRepository authorityRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void save(User user, Role role) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setAuthorities(new HashSet<>(Arrays.asList(authorityRepository.findByRole(role))));
        userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        try {
            userRepository.delete(id);
        }
        catch(IllegalArgumentException ex) {}
    }

    @Override
    public User find(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Page<User> findUsers(Role role, int page) {
        Pageable pageable = new PageRequest(page, PAGE_SIZE);
        return userRepository.findByAuthoritiesRoleEquals(role, pageable);
    }

    @Override
    public Page<User> findUsers(Role role, String phrase, int page) {
        Pageable pageable = new PageRequest(page, PAGE_SIZE);
        String phrasePrepared = "%"+phrase+"%";
        return userRepository.findByAuthoritiesRoleEqualsAndUsernameLikeIgnoringCase(role, phrasePrepared, pageable);
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User getLoggedInUser(Principal principal) throws UsernameNotFoundException, AuthenticationException {
        String username;
        User user;
        if(principal!=null && (username=principal.getName()) != null) {
            user = userRepository.findByUsername(username);
            if(user == null) {
                throw new UsernameNotFoundException("User " + username + " not found exception.");
            }
        }
        else {
            throw new AuthenticationException();
        }
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("User " + username + " not found exception.");
        }
        return user;
    }
}
