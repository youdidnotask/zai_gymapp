package com.gym.service;

import com.gym.exception.ResourceNotFoundException;
import com.gym.model.Activity;
import com.gym.model.Day;
import com.gym.repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Joanna Olejnik on 2017-05-26.
 */

@Service
public class ActivityServiceImpl implements ActivityService {

    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityServiceImpl(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @Override
    public void save(Activity activity) {
        activityRepository.save(activity);
    }

    @Override
    public void delete(Long id) {
        activityRepository.delete(id);
    }

    @Override
    public Activity find(Long id) {
        return activityRepository.findOne(id);
    }

    @Override
    public List<Activity> findByDay(Day day) {
        return (List<Activity>)activityRepository.findByDayOrderByHour(day);
    }

}
