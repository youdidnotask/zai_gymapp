package com.gym.service;

import com.gym.model.Activity;
import com.gym.model.Day;

import java.util.List;

/**
 * Created by Joanna Olejnik on 2017-05-26.
 */
public interface ActivityService {
    void save(Activity activity);
    void delete(Long id);
    Activity find(Long id);
    List<Activity> findByDay(Day day);
}
