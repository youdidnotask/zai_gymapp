package com.gym.repository;

import com.gym.model.Activity;
import com.gym.model.Day;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Joanna Olejnik on 2017-05-26.
 */
public interface ActivityRepository extends CrudRepository<Activity, Long> {
    Iterable<Activity> findByDayOrderByHour(Day day);
}
