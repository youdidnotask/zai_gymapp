package com.gym.repository;

import com.gym.model.Authority;
import com.gym.model.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Joanna Olejnik on 2017-05-22.
 */
public interface AuthorityRepository extends CrudRepository<Authority, Long> {
    Authority findByRole(Role role);
}
