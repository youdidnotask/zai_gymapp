package com.gym.repository;

import com.gym.model.Role;
import com.gym.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Joanna Olejnik on 2017-05-18.
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByUsername(String username);
    Page<User> findByAuthoritiesRoleEquals(Role role, Pageable pageable);
    Page<User> findByAuthoritiesRoleEqualsAndUsernameLikeIgnoringCase(Role role, String phrase, Pageable pageable);
}
